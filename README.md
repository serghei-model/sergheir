# sergheiR

R package tools for SERGHEI

## Requirements

To compile and use the SERGHEI R package, make sure the following packages are installed in your R system:
1. devtools 
2. ggplot2 
3. raster 
4. rasterVis
5. stringi
6. latex2exp
7. ncdf4 
8. parallel
9. ggforce
10. units
11. plyr
12. stringr
13. matrixStats
14. readr

## Compilation
### RStudio
If you have access to the RStudio GUI
1. File -> Open project -> select the `sergheiR.proj` file
2. Build -> Install and restart

### Command line
If you need to compile via the command line, navigate to the root directory of the SERGHEI R package, and use the **buildSerghei.R** script

```
$ Rscript buildSerghei.R
```

## Usage
You can call the package by using in your R code 
`library(SERGHEI)`

If you experience problems to build the package, you can always `source` the package's R files in your R script
```
source(/path/to/sergheiR/R/ReadSERGHEI.R)
source(/path/to/sergheiR/R/PlotSERGHEI.R)
```
