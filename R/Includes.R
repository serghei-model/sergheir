#' @import ggplot2
#' @import rasterVis
#' @import raster
#' @import latex2exp
#' @import ncdf4
#' @import parallel
#' @import ggforce
#' @import units
#' @import plyr
#' @import stringr
#' @import matrixStats
#' @import scales
